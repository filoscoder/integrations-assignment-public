import CONFIG from "@/config";
import axios, { AxiosRequestConfig } from "axios";

const { REVO } = CONFIG;

const baseInstance = axios.create({
    baseURL: REVO.BASE_URL,
    headers: {
        tenant: REVO.ACCOUNT,
        Authorization: `Bearer ${REVO.THE_TOKEN}`,
        ["client-token"]: REVO.CLIENT_TOKEN
    }
});

baseInstance.interceptors.response.use((res) => res.data);

type ResponseData<T = undefined> = false extends (
    T extends undefined ? true : false
)
    ? {
          data: T;
      }
    : null;

const apiRequest = {
    get: <T = undefined>(url: string, request?: AxiosRequestConfig) =>
        baseInstance.get<T, ResponseData<T>>(url, request),
    delete: <T = undefined>(url: string, request?: AxiosRequestConfig) =>
        baseInstance.delete<T, ResponseData<T>>(url, request),
    post: <T = undefined>(
        url: string,
        request?: unknown,
        config?: AxiosRequestConfig
    ) => baseInstance.post<T, ResponseData<T>>(url, request, config),
    put: <T = undefined>(
        url: string,
        request?: unknown,
        config?: AxiosRequestConfig
    ) => baseInstance.put<T, ResponseData<T>>(url, request, config),
    patch: <T = undefined>(
        url: string,
        request?: unknown,
        config?: AxiosRequestConfig
    ) => baseInstance.patch<T, ResponseData<T>>(url, request, config)
};

export default apiRequest;
