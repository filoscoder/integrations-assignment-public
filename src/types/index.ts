type IntString = `${number}`;
type HexString = `#${number}`;

interface Table {
    id: number;
    name: string;
    x: IntString;
    y: IntString;
    width: IntString;
    height: IntString;
    baseX: IntString;
    baseY: IntString;
    isJoined: IntString;
    baseWidth: IntString;
    baseHeight: IntString;
    color: HexString;
    type_id: IntString;
    room_id: IntString;
    price_id: IntString | null;
}

export interface Room {
    id: string;
    name: string;
    order: IntString;
    active: IntString;
    tables: Table[];
}
