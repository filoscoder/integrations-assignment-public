import HttpStatus, { NOT_FOUND, INTERNAL_SERVER_ERROR } from "http-status/lib";
import { NotFoundException } from "@/helpers";
import type { Request, Response, NextFunction } from "express";

export const notFoundError = (
    _req: Request,
    res: Response,
    _next: NextFunction
): void => {
    res.status(NOT_FOUND).json(new NotFoundException());
};

export const globalErrorHandler = (
    err: any,
    _req: Request,
    res: Response,
    _next: NextFunction
): void => {
    const statusCode: number = HttpStatus[err.statusCode]
        ? err.statusCode
        : INTERNAL_SERVER_ERROR;

    const resBody = {
        statusCode,
        message: err.message
    };

    res.status(statusCode).json(resBody);
};
