import { revoRoutes } from "@/components/revo/revo.routes";
import { auth } from "@/middlewares/auth";
import { Router } from "express";

export const routes = Router();

routes.use(auth, revoRoutes);

export default routes;
