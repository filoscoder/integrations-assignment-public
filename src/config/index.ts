import path from "path";
import pkg from "../../package.json";

const currentEnv = process.env.NODE_ENV || "development";
const envPath = path.resolve(__dirname, `../../dotenv/.env.development`);

// https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
require("dotenv").config({
    path: envPath
});

const CONFIG = {
    APP: {
        NAME: pkg.name,
        VERSION: pkg.version,
        DESCRIPTION: pkg.description,
        AUTHORS: pkg.author,
        HOST: process.env.HOST || "localhost",
        PORT: process.env.PORT || 0,
        ENV: currentEnv
    },
    SERVER: {
        PREFIX: "/api",
        SIZE_LIMIT: "1mb",
        SECRET_TOKEN: process.env.SECRET_TOKEN || ""
    },
    REVO: {
        BASE_URL: process.env.REVO_BASE_URL,
        ACCOUNT: process.env.REVO_ACCOUNT,
        THE_TOKEN: process.env.REVO_THE_TOKEN,
        CLIENT_TOKEN: process.env.REVO_CLIENT_TOKEN
    }
};

export default CONFIG;
