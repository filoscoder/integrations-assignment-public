import { createApp } from "@/app";
import supertest, { type SuperAgentTest } from "supertest";
import CONFIG from "@/config";
import { FoodlusZoneModel, FoodlusTableModel } from "./revo.model";

const { PREFIX, SECRET_TOKEN } = CONFIG.SERVER;

let agent: SuperAgentTest;
beforeAll(async () => {
    const app = createApp();
    agent = supertest.agent(app);
});

describe("{GET} /tables", () => {
    it("Should retrieve all available Zones with Tables", async () => {
        const { statusCode, body } = await agent
            .get(`${PREFIX}/tables`)
            .auth(SECRET_TOKEN, { type: "bearer" });

        const zone: FoodlusZoneModel = body[0];
        const table: FoodlusTableModel = zone.serviceLocations[0];

        expect(statusCode).toBe(200);
        expect(Array.isArray(body)).toBe(true);
        expect(zone).toHaveProperty("name");
        expect(zone).toHaveProperty("serviceLocations");
        expect(Array.isArray(zone.serviceLocations)).toBe(true);
        expect(table).toHaveProperty("name");
        expect(table).toHaveProperty("code");
        expect(table).toHaveProperty("zoneId");
    });
});
