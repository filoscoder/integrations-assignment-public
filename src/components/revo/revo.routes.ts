import { getTables } from "./revo.controller";
import { Router } from "express";

export const revoRoutes = Router();

revoRoutes.get("/tables", getTables);
