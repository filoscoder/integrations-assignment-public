import { FoodlusZoneModel } from "./revo.model";
import { RevoAPI } from "./revo.repository";

export class RevoService {
    revoApi: RevoAPI;
    constructor() {
        this.revoApi = new RevoAPI();
    }

    async getRevoTables(): Promise<Array<FoodlusZoneModel>> {
        const rooms = await this.revoApi.getRevoRooms();

        const zonesWithServiceLocations = rooms.map(({ id, name, tables }) => {
            const serviceLocations = tables?.map((table) => {
                return {
                    name: table.name,
                    code: table.id,
                    zoneId: id,
                    zoneName: name
                };
            });
            const zone = {
                name,
                serviceLocations
            };

            return zone;
        });

        return zonesWithServiceLocations;
    }
}
