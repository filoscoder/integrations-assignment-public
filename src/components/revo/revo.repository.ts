import apiRequest from "@/api";
import { Room } from "@/types";

export class RevoAPI {
    async getRevoRooms() {
        const { data } = await apiRequest.get<Array<Room>>("/rooms", {
            params: {
                withTables: true
            }
        });

        // if (!data) <= Need handling

        return data;
    }
}
