import { OK } from "http-status/lib";
import type { Request, Response, NextFunction } from "express";
import { RevoService } from "./revo.service";

export const getTables = async (
    _req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        const revoService = new RevoService();
        const result = await revoService.getRevoTables();

        res.status(OK).json(result);
    } catch (error) {
        console.log(error);
        next(error);
    }
};
