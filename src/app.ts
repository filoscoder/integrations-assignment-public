import express, { type Application } from "express";
import morgan from "morgan";
import cors from "cors";
import routes from "./routes";
import { globalErrorHandler, notFoundError } from "./middlewares";
import CONFIG from "./config";

const { PREFIX, SIZE_LIMIT } = CONFIG.SERVER;

export const createApp = (): Application => {
    const app = express();

    app.use(morgan("dev"));
    app.use(cors());
    app.use(express.json({ limit: SIZE_LIMIT }));
    app.use(
        express.urlencoded({
            extended: true,
            limit: SIZE_LIMIT
        })
    );

    app.use(PREFIX, routes);
    app.use(globalErrorHandler);
    app.use(notFoundError);

    return app;
};
