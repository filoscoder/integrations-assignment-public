# Foodlus Assignment
Forked from [here](https://gitlab.com/gorkavillar/integrations-assignment-public)

## Getting Started

#### 1. Install Dependencies

```bash
$ npm i
```

#### 2. Setup dot env file(s)

Place below content at this path:
- `/dotenv/.env.development`

```bash
NODE_ENV=development
HOST=localhost
PORT=8080
SECRET_TOKEN=dummy_foodlus_secret_token
REVO_BASE_URL=https://revoxef.works/api/external/v2
REVO_ACCOUNT=foodlus
REVO_THE_TOKEN=YNgM9bwS9pqnLePP
REVO_CLIENT_TOKEN=gy5oHFUJCdKFqLtb740431EEe4FB1bThdfPwWZre5LMm1BtBTEIcgJ4Vy9RI
```

#### 3. Run on dev mode

```bash
$ npm run dev # Running at http://localhost:8080/
```

## API

#### Authorization
```bash
Bearer {SECRET_TOKEN}
```
Error: 401 (Unauthorized)

#### /tables
- {`GET`} Should retrieve all available Zones with Tables
  - Example: 200
  ```json
  [
    {
        "name": "Room 1",
        "serviceLocations": [
            {
                "name": "Table 1",
                "code": 1,
                "zoneId": 1,
                "zoneName": "Room 1"
            }
        ]
    },
  ]
  ```